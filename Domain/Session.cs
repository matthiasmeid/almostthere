using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace almost_there.Domain
{
    public sealed class Session
    {
        private Dictionary<Participant, Vote> votes = new Dictionary<Participant, Vote>();

        public Session(int sessionId)
        {
            SessionId = sessionId;
            Mode = Mode.VotesHidden;
        }

        public void Join(Participant participant)
        {
            this.votes.TryAdd(participant, Domain.Vote.None);
        }

        public void Vote(Participant participant, Vote vote)
        {
            this.votes[participant] = vote;
        }

        public void ClearAll()
        {
            this.votes = new Dictionary<Participant, Vote>(this.votes.Keys.Select(k => new KeyValuePair<Participant, Vote>(k, Domain.Vote.None)));
            this.Mode = Mode.VotesHidden;
        }

        public void ShowAll()
        {
            this.Mode = Mode.CardsOnTable;
        }

        public int SessionId { get; }

        public ImmutableDictionary<Participant, Vote> Votes => votes.ToImmutableDictionary();

        public ImmutableList<Participant> Participants => votes.Keys.ToImmutableList();

        public Mode Mode { get; private set; }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            var other = (Session)obj;
            return SessionId == other.SessionId;
        }

        public override int GetHashCode()
        {
            return SessionId;
        }
    }

    public enum Mode
    {
        Unknown,
        VotesHidden,
        CardsOnTable
    }
}