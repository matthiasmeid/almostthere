namespace almost_there.Controllers
{
    public class JoinAction
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}";
        }
    }
}