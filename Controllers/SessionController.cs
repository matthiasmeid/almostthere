using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using almost_there.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace almost_there.Controllers
{
    [Route("api/Session/")]
    public class SessionController : Controller
    {
        private static ImmutableHashSet<Session> sessions = ImmutableHashSet<Session>.Empty;

        private readonly IHubContext<SessionUpdateHub, ISessionUpdateHubClient> hubContext;

        public SessionController(IHubContext<SessionUpdateHub, ISessionUpdateHubClient> hubContext)
        {
            this.hubContext = hubContext;
        }

        [HttpPost("{sessionId}/joinAction")]
        public SessionSnapshot Join(int sessionId, [FromBody] JoinAction joinAction)
        {
            var session = GetOrCreateSession(sessionId);

            session.Join(new Participant(joinAction.Name));

            return ReturnSnapshotAndNotifyClients(session);
        }

        [HttpPost("{sessionId}/voteAction")]
        public void Vote(int sessionId, [FromBody] VoteAction voteAction)
        {
            var session = GetOrCreateSession(sessionId);

            session.Vote(new Participant(voteAction.Name), Domain.Vote.Create(voteAction.Vote));
            
            ReturnSnapshotAndNotifyClients(session);
        }

        [HttpPost("{sessionId}/showAllAction")]
        public void ShowAll(int sessionId)
        {
            var session = GetOrCreateSession(sessionId);

            session.ShowAll();
            
            ReturnSnapshotAndNotifyClients(session);
        }

        [HttpPost("{sessionId}/clearAllAction")]
        public void ClearAll(int sessionId)
        {
            var session = GetOrCreateSession(sessionId);

            session.ClearAll();
            
            ReturnSnapshotAndNotifyClients(session);
        }

        private static Session GetOrCreateSession(int sessionId)
        {
            var session = sessions.FirstOrDefault(s => s.SessionId == sessionId);

            if (session == null)
            {
                session = new Session(sessionId);
                sessions = sessions.Add(session);
            }

            return session;
        }

        private SessionSnapshot ReturnSnapshotAndNotifyClients(Session session)
        {
            var sessionSnapshot = SessionSnapshot.FromSession(session);

            this.hubContext.Clients
                .Group(session.SessionId.ToString())
                .BroadcastUpdate(sessionSnapshot);

            return sessionSnapshot;
        }
    }
}
