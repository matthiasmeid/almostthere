namespace almost_there.Controllers
{
    public class VoteAction
    {
        public string Name { get; set; }

        public string Vote { get; set; }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(Vote)}: {Vote}";
        }
    }
}