using System.Threading.Tasks;
using almost_there.Controllers;

namespace almost_there
{
    public interface ISessionUpdateHubClient
    {
        Task BroadcastUpdate(SessionSnapshot sessionSnapshot);
    }
}