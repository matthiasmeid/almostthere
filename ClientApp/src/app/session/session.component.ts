import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable, combineLatest } from 'rxjs';

import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class SessionComponent implements OnInit, OnDestroy {

  readonly votingOptions: string[] = ['0.5', '1', '2', '3', '5', '8', '13', '21', '34', '55', '?'];

  id: number;
  name: string;

  private sessionSnapshot: SessionSnapshot = new SessionSnapshot();

  private subscriptions: any[] = [];
  private hubConnection: HubConnection;

  constructor(private route: ActivatedRoute, private http: HttpClient, @Inject("BASE_URL") private baseUrl: string) {
  }

  participants(): string[] { return this.sessionSnapshot.participants };

  vote(vote: string) {
    console.log(vote);

    this.subscriptions.push(this.http
      .post<SessionSnapshot>(
        this.baseUrl + 'api/Session/' + this.id + '/voteAction',
        { name: this.name, vote: vote })
      .subscribe(
        r => { },
        err => { console.error(err) }
      ));
  }

  clearAll() {
    console.log("clear all");

    this.subscriptions.push(this.http
      .post<SessionSnapshot>(
        this.baseUrl + 'api/Session/' + this.id + '/clearAllAction',
        {})
      .subscribe(
        r => { },
        err => { console.error(err) }
      ));
  }

  showAll() {
    console.log("show all");

    this.subscriptions.push(this.http
      .post<SessionSnapshot>(
        this.baseUrl + 'api/Session/' + this.id + '/showAllAction',
        {})
      .subscribe(
        r => { },
        err => { console.error(err) }
      ));
  }

  hasVoted(name: string): boolean {
    return this.sessionSnapshot.votes[name] || false;
  }

  voteByName(name: string): string {
    return this.sessionSnapshot.mode == 'CardsOnTable' || name == this.name
      ? this.sessionSnapshot.votes[name]
      : null;
  }

  lastVote() {
    return this.sessionSnapshot.votes[this.name];
  }

  ngOnInit() {

    this.subscriptions.push(combineLatest(this.route.params, this.route.queryParams).subscribe(([ps, qps]) => {
      this.id = +ps['id'];
      this.name = qps.name;

      this.subscriptions.push(this.http
        .post<any>(this.baseUrl + 'api/Session/' + this.id + '/joinAction', { name: this.name })
        .subscribe(
          r => {

            this.sessionSnapshot = r;
            
            this.hubConnection = new HubConnectionBuilder()
              .withUrl(this.baseUrl + 'sessionUpdates?sessionId=' + this.id)
              .build();

            this.hubConnection
              .start()
              .then(() => { console.log("hub connection started") })
              .catch(err => console.log(err));

            this.subscriptions.push(this.hubConnection.on('BroadcastUpdate', data => {
              this.sessionSnapshot = data;
            }));
          },
          err => { console.error(err) }
        )
      );
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}

class SessionSnapshot {
  participants: string[] = [];
  votes: any = {};
  mode: 'VotesHidden' | 'CardsOnTable';
}