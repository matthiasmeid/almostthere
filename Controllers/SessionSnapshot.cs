using System.Collections.Immutable;
using System.Linq;
using almost_there.Domain;

namespace almost_there.Controllers
{
    public class SessionSnapshot
    {
        public static SessionSnapshot FromSession(Session session)
        {
            return new SessionSnapshot
            {
                Participants = session.Participants.OrderBy(p => p.Name).Select(p => p.Name).ToImmutableList(),
                Votes = session.Votes.ToImmutableDictionary(p => p.Key.Name, p => p.Value.ToString()),
                Mode = session.Mode
            };
        }

        public Mode Mode { get; set; }

        public ImmutableList<string> Participants { get; set; }

        public ImmutableDictionary<string, string> Votes { get; set; }
    }
}