using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace almost_there
{
    public class SessionUpdateHub : Hub<ISessionUpdateHubClient>
    {
        public override async Task OnConnectedAsync()
        {
            var sessionIdQuery = Context.GetHttpContext().Request.Query["SessionId"].FirstOrDefault();

            if (int.TryParse(sessionIdQuery, out var sessionId))
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, sessionId.ToString());
            }

            await base.OnConnectedAsync();
        }
    }
}