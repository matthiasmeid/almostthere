using System;
using System.Collections.Immutable;

namespace almost_there.Domain
{
    public struct Vote
    {
        private static readonly ImmutableList<string> ValidVotes = ImmutableList.Create("0.5", "1", "2", "3", "5", "8", "13", "21", "34", "55", "?");
        
        public static readonly Vote None = new Vote(string.Empty);
        
        private readonly string vote;

        private Vote(string vote)
        {
            this.vote = vote ?? throw new ArgumentNullException(nameof(vote));
        }

        public override string ToString()
        {
            return this.vote;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Vote other && string.Equals(vote, other.vote, StringComparison.OrdinalIgnoreCase);
        }

        public override int GetHashCode()
        {
            return (vote != null ? StringComparer.OrdinalIgnoreCase.GetHashCode(vote) : 0);
        }

        public static bool operator ==(Vote left, Vote right) => string.Equals(left.vote, right.vote, StringComparison.OrdinalIgnoreCase);

        public static bool operator !=(Vote left, Vote right) => !string.Equals(left.vote, right.vote, StringComparison.OrdinalIgnoreCase);
        
        public static Vote Create(string vote)
        {
            if (ValidVotes.Contains(vote))
            {
                return new Vote(vote);
            }
            
            throw new ArgumentOutOfRangeException(nameof(vote));
        }
    }
}